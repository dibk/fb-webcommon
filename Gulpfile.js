// Include gulp
var gulp = require('gulp');

// Include Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var del = require('del');

var config = require('./gulp-config.json');

gulp.task('scripts', function() {
    return gulp.src(config.paths.scripts)
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./assets/js/'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('styles', function() {
    return gulp.src(config.paths.stylesheets)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(cssnano())
        .pipe(gulp.dest('./assets/css/'));
});

gulp.task('fonts', function() {
    return gulp.src(config.paths.fonts)
        .pipe(gulp.dest('./assets/fonts/'));
});

gulp.task('images', function() {
    return gulp.src(config.paths.images)
        .pipe(gulp.dest('./assets/images/'));
});


gulp.task('demo-images', function() {
    return gulp.src(config.paths.images)
        .pipe(gulp.dest(config.paths.demo.assets + 'images/'));
});

gulp.task('demo-fonts', function() {
    return gulp.src(config.paths.fonts)
        .pipe(gulp.dest(config.paths.demo.assets + 'fonts/'));
});

gulp.task('demo', function(){
    gulp.start('demo-images', 'demo-fonts');
});


// Clean
gulp.task('clean', function() {
    return del(['./assets/css', './assets/js', './assets/fonts', './assets/images']);
});


// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'fonts', 'images');
    gulp.watch('src/sass/**/*.scss', ['styles']);
    gulp.watch('src/js/**/*.js', ['scripts']);
});