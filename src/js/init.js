function closeToggleableElements(toggleableElements){
    console.log(toggleableElements);
    for (var i = 0; i < toggleableElements.length; i++){
        var toggleableElement = toggleableElements[i];
        toggleableElement.classList.remove('active');
    }
}


document.addEventListener('DOMContentLoaded', function() {
    var toggleElements = document.getElementsByClassName('content-toggle') !== undefined ? document.getElementsByClassName('content-toggle') : false;
    var toggleableElements = document.getElementsByClassName('toggleableContent');
    
    if (toggleElements){
        for (var i = 0; i < toggleElements.length; i++) {
            var toggleElement = toggleElements[i];
            toggleElement.addEventListener('click', function(event){
                var targetElementId = event.target.getAttribute('data-content-toggle');
                var targetElement = document.getElementById(targetElementId);
                
                var alreadyActive = false;
                targetElementClasses = targetElement.classList;
                targetElementClasses.forEach(function(targetElementClass){
                    if (targetElementClass == 'active') alreadyActive = true;
                });

                closeToggleableElements(toggleableElements);
                if (!alreadyActive) targetElement.classList.add('active');
            });
        }
    }

    var toggleMenuElement = document.getElementById('menu-toggle') !== undefined ? document.getElementById('menu-toggle') : false;
    if (toggleMenuElement){
        toggleMenuElement.addEventListener('click', function(event){
            var menuElement = document.getElementById('main-menu') !== undefined ? document.getElementById('main-menu') : false;
            if (menuElement) menuElement.classList.toggle('active');
        })
    }


}, false);